import gitlab
import os


url = 'https://gitlab.com'
id = os.environ['CI_PROJECT_ID']

def get_or_create_trigger(project):
    trigger_decription = 'my_trigger_id'
    for t in project.triggers.list():
        if t.description == trigger_decription:
            return t
    return project.triggers.create({'description': trigger_decription})

# private token or personal token authentication
gl = gitlab.Gitlab(url, private_token=os.environ['CI_PRI_TOKEN'])

#projects = gl.projects.liast()
#for project in projects:
#    print(project)

project = gl.projects.get(id)
print(project.attributes)



#trigger = get_or_create_trigger(project)
#pipeline = project.trigger_pipeline('master', trigger.token, variables={"DEPLOY_ZONE": "us-west1"})
#while pipeline.finished_at is None:
#    pipeline.refresh()
#    time.sleep(1)
